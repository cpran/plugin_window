# Setup script for window
#
# This script is part of the <plugin> CPrAN plugin for Praat.
# The latest version is available through CPrAN or at
# http://cpran.net/plugins/window
#
# The window plugin is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# The window plugin is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with utils. If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2017 Jose Joaquin Atria

## Static commands:

## Dynamic commands:
