# This script is part of the window CPrAN plugin for Praat.
# The latest version is available through CPrAN or at
# <http://cpran.net/plugins/window>
#
# The window plugin is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# The window plugin is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the window plugin. If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2017 Jose Joaquin Atria

#
# Hooks
#

# Main action procedure
# Called once per iteration, this holds the entire iteration logic
procedure window.action ()
endproc

# Called as the first thing that happens before entering the main loop
procedure window.before_iteration ()
endproc

# Called as the last thing that happens after leaving the main loop
procedure window.after_iteration ()
endproc

#! ~~~ params
#! in:
#!   .mode$: The window specification mode, either "duration" or "number"
#!   .value: The window specification value
#!   .overlap: The amount of overlap between windows
#! ~~~
#!
#!
#!
procedure window: .mode$, .value, .overlap
  if !variableExists("window.elasticity")
    window.elasticity = 0.000001
  endif

  if .overlap == 1
    exit Windows cannot completely overlap!
  endif

  .object          = selected()
  .object_start    = Get start time
  .object_end      = Get end time
  .object_duration = Get total duration

  if .mode$ == "number"
    .ideal_duration = .object_duration / (.value - (.overlap * (.value - 1)))
  elsif .mode$ == "duration"
    .ideal_duration = .value
  else
    exit Unknown mode. Valid modes are "number" or "duration"
  endif

  .shift  = .ideal_duration * (1 - .overlap)

  @window.before_iteration()

  .index = 0
  repeat

    selectObject: .object

    if .overlap < 1
      .start = .object_start + (.shift * (.index))
      .end = .start + .ideal_duration
      .end = if .end > .object_end then .object_end else .end fi
    else
      .end = .object_end + (.shift * (.index))
      .start = .end - .ideal_duration
      .start = if .start < .object_start then .object_start else .start fi
    endif

    .index += 1

    if window.elasticity
      .remainder = if .overlap < 1
      ...          then .object_end - .end else .object_start + .start fi

      if .remainder and .remainder < .ideal_duration * window.elasticity
        if .overlap < 1
          .end += .remainder
        else
          .start -= .remainder
        endif
      endif
    endif

    .duration = .end - .start

    @window.action()

  until if .overlap < 1 then .end >= .object_end else .start <= .object_start fi

  @window.after_iteration()
endproc
