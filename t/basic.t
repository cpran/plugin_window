include ../../plugin_tap/procedures/more.proc

@no_plan()

if praatVersion >= 6036
  synth_language$ = "English (Great Britain)"
  synth_voice$ = "Male1"
else
  synth_language$ = "English"
  synth_voice$ = "default"
endif

synth = Create SpeechSynthesizer: synth_language$, synth_voice$
To Sound: "This is some text.", "yes"

procedure window.before_iteration ()
  mean = 0
#   @diag: "Object: 'window.object_start' to 'window.object_end'"
#   @diag: "Window shift: 'window.shift'"
#   @diag: "Ideal duration: 'window.ideal_duration'"
endproc

procedure window.action ()
#   @diag: "Window 'window.index' from 'window.start' to 'window.end' for 'window.duration'"
  mean += window.duration
endproc

procedure window.after_iteration ()
  mean /= window.index
endproc

include ../../plugin_window/procedures/window.proc

sound    = selected("Sound")
textgrid = selected("TextGrid")

selectObject: sound
object_duration = Get total duration
window_number = 10
window_duration = 0.1

overlap = 0
@window: "number", window_number, overlap
@is: window.index, window_number, "Window by number, no overlap"
@check_mean_duration: overlap

overlap = 0.25
@window: "number", window_number, overlap
@is: window.index, window_number, "Window number accounts for overlap"
@check_mean_duration: overlap

overlap = 0
@window: "duration", window_duration, overlap
@is: window.index, ceiling(object_duration / window_duration),
  ... "Window by duration, no overlap"

overlap = 0.25
@window: "duration", window_duration, overlap
@is: window.index, ceiling(
  ...   (object_duration - window_duration) /
  ...   (window_duration * (1 - overlap)) + 1
  ... ),
  ... "Window by duration with overlap"

overlap = 2
@window: "duration", window_duration, overlap
@is: window.index, ceiling(object_duration / window_duration),
  ... "Window by duration, overlap going backwards"

@done_testing()

procedure check_mean_duration: .overlap
  @cmp_ok: mean - (object_duration / (window_number -
    ...      (.overlap * (window_number - 1)))),
    ... "<", 0.0001,
    ... "Window mean by number, overlap = " + string$(.overlap)
endproc
